<?php

/**
 * @file
 * Contains html_head_meta_and_link.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function html_head_meta_and_link_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the html_head_meta_and_link module.
    case 'help.page.html_head_meta_and_link':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Provides the ability to unset html head metas and links') . '</p>';
      return $output;

    default:
  }
}

/**
* Implements hook_entity_view_alter().
*/
function html_head_meta_and_link_entity_view_alter(array &$build, Drupal\Core\Entity\EntityInterface $entity, \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display) {
	// Cheking view_mode for node.
	if ($build['#view_mode'] === 'full' && $entity Instanceof \Drupal\node\NodeInterface) {
	    html_head_meta_and_link_unset($build['#attached']['html_head_link'], 'html_head_link');
	}
}

/**
 * Implements hook_page_attachments_alter().
 */
function html_head_meta_and_link_page_attachments_alter(array &$attachments) {
    html_head_meta_and_link_unset($attachments['#attached']['html_head'], 'html_head');
}

/**
 * Implements hook_module_implements_alter().
 */
function html_head_meta_and_link_module_implements_alter(&$implementations, $hook) {
  if ($hook === 'page_attachments_alter') {
    $group = $implementations['html_head_meta_and_link'];
    unset($implementations['html_head_meta_and_link']);
    $implementations['html_head_meta_and_link'] = $group;
  }
}


function html_head_meta_and_link_unset(array &$attachments, string $type) {
    $elements_to_unset = [];

    // Get all config value of atinternet
    $config = \Drupal::config('html_head_meta_and_link.htmlheadmetaandlink');

    $metas_generator = $config->get('hhml_metas_generator');
    $metas_mobile_optimized = $config->get('hhml_metas_mobile_optimized');
    $metas_handheld_friendly = $config->get('hhml_metas_handheld_friendly');
    $links_shortlink = $config->get('hhml_links_shortlink');
    $links_delete_form = $config->get('hhml_links_delete_form');
    $links_edit_form = $config->get('hhml_links_edit_form');
    $links_version_history = $config->get('hhml_links_version_history');
    $links_revision = $config->get('hhml_links_revision');
    $links_replicate = $config->get('hhml_links_replicate');

    // Populate the array of elements to unset
    // Meta tags
    if($metas_generator) {
    	$elements_to_unset[] = 'system_meta_generator';
    }

    if($metas_mobile_optimized) {
    	$elements_to_unset[] = 'MobileOptimized';
    }

    if($metas_handheld_friendly) {
    	$elements_to_unset[] = 'HandheldFriendly';
    }

    // Link tags
    if($links_shortlink) {
    	$elements_to_unset[] = 'shortlink';
    }

    if($links_delete_form) {
    	$elements_to_unset[] = 'delete-form';
    }

    if($links_edit_form) {
    	$elements_to_unset[] = 'edit-form';
    }

    if($links_version_history) {
    	$elements_to_unset[] = 'version-history';
    }

    if($links_revision) {
    	$elements_to_unset[] = 'revision';
    }

    if($links_replicate) {
    	$elements_to_unset[] = 'replicate';
    }

	if($type == 'html_head') {
		foreach ($attachments as $key => $value) {
			if (in_array($value[1], $elements_to_unset)) {
				unset($attachments[$key]);
	    	}
    	}
	}
	else { // ie. html_head_link
		foreach ($attachments as $key => $value) {
			if (isset($value[0]['rel']) && in_array($value[0]['rel'], $elements_to_unset)) {
				unset($attachments[$key]);
	    	}
    	}
	}
}