<?php

namespace Drupal\html_head_meta_and_link\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Html head meta and link entity entities.
 */
interface HtmlHeadMetaAndLinkEntityInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
